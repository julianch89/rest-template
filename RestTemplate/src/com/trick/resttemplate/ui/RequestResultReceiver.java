package com.trick.resttemplate.ui;

import com.trick.resttemplate.infrastructure.rest.response.ResponseMessage;
import com.trick.resttemplate.infrastructure.rest.service.RequestIntentService;
import com.trick.resttemplate.services.RequestServiceHelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;



public class RequestResultReceiver extends BroadcastReceiver
{
	private Receiver mReceiver;
	private Context mContext;

	public RequestResultReceiver(Context context)
	{
		mContext = context;
	}

	public interface Receiver
	{
		public void onRequestSuccess(long requestId);

		public void onRequestError(long requestId, int resultCode, ResponseMessage msg);

		public void onRequestFinished(long requestId);
	}

	public void setReceiver(Receiver receiver)
	{
		IntentFilter filter = new IntentFilter(RequestServiceHelper.ACTION_REQUEST_RESULT);
		if (receiver == null)
			mContext.unregisterReceiver(this);
		else
			mContext.registerReceiver(this, filter);

		mReceiver = receiver;
	}


	@Override
	public void onReceive(Context context, Intent intent)
	{
		if (mReceiver == null)
			return;

		long resultRequestId = intent.getLongExtra(RequestServiceHelper.EXTRA_REQUEST_TRANSACTION_ID, 0);
		int resultCode = intent.getIntExtra(RequestServiceHelper.EXTRA_RESULT_CODE, 0);
		ResponseMessage msg = intent.getParcelableExtra(RequestServiceHelper.EXTRA_RESULT_MSG);

		if (resultCode == RequestIntentService.REQUEST_SUCCESS)
			mReceiver.onRequestSuccess(resultRequestId);
		else
			mReceiver.onRequestError(resultRequestId, resultCode, msg);

		mReceiver.onRequestFinished(resultRequestId);
	}

}