package com.trick.resttemplate.ui.adapters;

import com.trick.resttemplate.R;
import com.trick.resttemplate.infrastructure.data.table.ItemTable;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

public class ItemAdapter extends SimpleCursorAdapter {

	public ItemAdapter(Context context, int layout, Cursor c, String[] from, int[] to)
	{
		super(context, layout, c, from, to, CursorAdapter.FLAG_AUTO_REQUERY);
	}

	public View getView(int position, View convertView, ViewGroup parent)
	{
		View rowView = super.getView(position, convertView, parent);

		Cursor cursor = (Cursor) getItem(position);
		String status = cursor.getString(cursor.getColumnIndex(ItemTable.Column._STATUS));

		ImageView syncImage = (ImageView) rowView.findViewById(R.id.item_sync_image);

		if (status == null || status.equals(""))
		{
			syncImage.setVisibility(View.INVISIBLE);
			syncImage.setAnimation(null);
		}
		else
		{
			syncImage.setVisibility(View.VISIBLE);
			RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

			//Setup anim with desired properties
			anim.setRepeatCount(Animation.INFINITE); //Repeat animation indefinitely
			anim.setDuration(400); //Put desired duration per anim cycle here, in milliseconds

			//Start animation
			syncImage.startAnimation(anim); 
		}

		return rowView;
	}

}
