package com.trick.resttemplate.ui;

import com.trick.resttemplate.R;
import com.trick.resttemplate.RestTemplateApp;
import com.trick.resttemplate.infrastructure.data.table.ItemTable;
import com.trick.resttemplate.infrastructure.data.table.Table;
import com.trick.resttemplate.infrastructure.rest.response.ResponseMessage;
import com.trick.resttemplate.services.RequestServiceHelper;
import com.trick.resttemplate.ui.RequestResultReceiver.Receiver;
import com.trick.resttemplate.ui.adapters.ItemAdapter;

import android.app.ListActivity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ListAdapter;

public class MainActivity extends ListActivity implements Receiver {

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//TODO TESTING 
		Uri uri = Table.Items.getContentUri();
		String[] columns = new String[] { ItemTable.Column.ITEM_ID, ItemTable.Column.NAME, ItemTable.Column._ID, ItemTable.Column._STATUS };
		Cursor cursor = RestTemplateApp.getInstance().getContentResolver().query(uri, columns, null, null, null);


		 ListAdapter adapter = new ItemAdapter(this, R.layout.item, cursor,
		 new String[] { ItemTable.Column.ITEM_ID, ItemTable.Column.NAME }, new
		 int[] { R.id.item_id, R.id.item_name });
		// Bind to our new adapter.
		setListAdapter(adapter);

		//RequestServiceHelper.getItems();
		RequestServiceHelper.postItem();
	}

	@Override
	public void onRequestSuccess(long requestId)
	{

	}

	@Override
	public void onRequestError(long requestId, int resultCode, ResponseMessage msg)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onRequestFinished(long requestId)
	{
		// TODO Auto-generated method stub

	}

}
