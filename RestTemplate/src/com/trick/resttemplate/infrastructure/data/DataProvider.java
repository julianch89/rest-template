package com.trick.resttemplate.infrastructure.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

import com.trick.resttemplate.infrastructure.data.table.BaseTable;
import com.trick.resttemplate.infrastructure.data.table.Table;

public class DataProvider extends ContentProvider {

	public static final String AUTHORITY = "com.trick.resttemplate.infrastructure.data.DataProvider";

	private ProviderDbHelper dbHelper;
	private UriMatcher uriMatcher;

	@Override
	public boolean onCreate()
	{
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		for (Table table : Table.values())
		{
			uriMatcher.addURI(AUTHORITY, table.getTableName(), table.GetTableMatch());
			uriMatcher.addURI(AUTHORITY, table.getTableName() + "/#", table.GetRowMatch());
		}

		this.dbHelper = new ProviderDbHelper(this.getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
	{

		int uriMatch = uriMatcher.match(uri);
		BaseTable table = Table.getTableFromUriMatch(uriMatch).getTable();
		if (isRowUri(uriMatch))
		{
			long id = Long.parseLong(uri.getPathSegments().get(1));
			selection = appendRowId(table, selection, id);
		}

		SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor = db.query(table.getTableName(), projection, selection, selectionArgs, null, null, sortOrder);

		// Tell the cursor what uri to watch, so it knows when its source data
		// changes
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public String getType(Uri uri)
	{
		// I hope we won't need this
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values)
	{
		int uriMatch = uriMatcher.match(uri);

		if (isRowUri(uriMatch))
		{
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		BaseTable table = Table.getTableFromUriMatch(uriMatch).getTable();

		String tableName = table.getTableName();
		long id = db.insertOrThrow(tableName, null, values);
		// TODO [Refactor] Add some error handling/logging

		// Notify any watchers of the change
		Uri newUri = ContentUris.withAppendedId(table.getContentUri(), id);
		getContext().getContentResolver().notifyChange(newUri, null);
		return newUri;

	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs)
	{
		SQLiteDatabase db = this.dbHelper.getWritableDatabase();
		int deletedRowsCount = 0;

		// Perform the delete based on URI pattern
		int uriMatch = uriMatcher.match(uri);
		BaseTable table = Table.getTableFromUriMatch(uriMatch).getTable();

		if (isRowUri(uriMatch))
		{
			long id = Long.parseLong(uri.getPathSegments().get(1));
			selection = appendRowId(table, selection, id);
		}
		deletedRowsCount = db.delete(table.getTableName(), selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);

		return deletedRowsCount;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
	{
		int uriMatch = uriMatcher.match(uri);

		if (!isRowUri(uriMatch))
		{
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		BaseTable table = Table.getTableFromUriMatch(uriMatch).getTable();
		String recordId = Long.toString(ContentUris.parseId(uri));

		int affected = db.update(table.getTableName(), values, table.getId() + "=" + recordId
				+ (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);

		getContext().getContentResolver().notifyChange(uri, null);
		return affected;
	}

	// TODO [Refactor] Make this condition less coupled
	private boolean isRowUri(int uriMatch)
	{
		return uriMatch % 2 == 1;
	}

	/**
	 * Append an id test to a SQL selection expression
	 */
	private String appendRowId(BaseTable table, String selection, long id)
	{
		return table.getId() + "=" + id + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] allValues)
	{
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		int numInserted = 0;
		String tableName = Table.getTableFromUriMatch(uriMatcher.match(uri)).getTableName();

		db.beginTransaction();
		try
		{
			for (ContentValues cv : allValues)
			{
				long newID = db.insertWithOnConflict(tableName, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
				if (newID <= 0)
					throw new SQLException("Error to add: " + uri);
			}
			db.setTransactionSuccessful();
			getContext().getContentResolver().notifyChange(uri, null);
			numInserted = allValues.length;
		} finally
		{
			db.endTransaction();
		}
		return numInserted;
	}

	

	public int insertOrUpdate(Uri uri, ContentValues[] allValues)
	{
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		int numAffected = 0;
		BaseTable table = Table.getTableFromUriMatch(uriMatcher.match(uri)).getTable();

		db.beginTransaction();
		try
		{
			for (ContentValues cv : allValues)
			{
				long newID;
				if (isAlreadyStored(db, uri, cv))
				{

					db.updateWithOnConflict(table.getTableName(), cv, table.getId() + "=?",
							new String[] { String.valueOf(cv.get(table.getId())) }, SQLiteDatabase.CONFLICT_REPLACE);

				} else
				{
					newID = db.insertWithOnConflict(table.getTableName(), null, cv, SQLiteDatabase.CONFLICT_REPLACE);
					if (newID <= 0)
						throw new SQLException("Error to add: " + uri);
				}
			}
			db.setTransactionSuccessful();
			getContext().getContentResolver().notifyChange(uri, null);
			numAffected = allValues.length;
		} finally
		{
			db.endTransaction();
		}
		return numAffected;
	}

	private boolean isAlreadyStored(SQLiteDatabase db, Uri uri, ContentValues cv)
	{

		BaseTable table = Table.getTableFromUriMatch(uriMatcher.match(uri)).getTable();
		// TODO [Refactor] Take care if id is different of integer
		String id = String.valueOf(cv.get(table.getId()));
		Cursor cursor = db.query(table.getTableName(), null, table.getId() + "=?", new String[] { id }, null, null, null);
		return cursor.moveToFirst();

	}
}
