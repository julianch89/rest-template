package com.trick.resttemplate.infrastructure.data;


import com.trick.resttemplate.infrastructure.data.table.Table;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This creates, updates, and opens the database. Opening is handled by the superclass, we handle the create & upgrade
 * steps
 */

public class ProviderDbHelper extends SQLiteOpenHelper{
	
	
	public final String TAG = getClass().getSimpleName();

	private static final String DATABASE_NAME = "rest.db";
	private static final int DATABASE_VERSION = 1;
	
	public ProviderDbHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		Log.i(TAG, "Creating Item table");
		db.execSQL(Table.Items.getTable().createSQL());
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		// Gets called when the database is upgraded, i.e. the version number changes
		
	}

}
