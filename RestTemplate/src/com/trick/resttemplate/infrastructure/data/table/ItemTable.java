package com.trick.resttemplate.infrastructure.data.table;

public class ItemTable extends BaseTable {

	public static class Column implements ResourceTable
	{
		public static final String ITEM_ID = "item_id";
		public static final String NAME = "name";
	}
	
	@Override
	public String getTableName()
	{
		return "items";
	}

	@Override
	public String getId()
	{
		return Column.ITEM_ID;
	}
	
	@Override
	protected void aditionalColumnsStatement(StringBuilder creationStatement)
	{
		 creationStatement.append(Column.NAME + " TEXT, ");
		 creationStatement.append(Column.ITEM_ID + " INTEGER"); 
	}

}
