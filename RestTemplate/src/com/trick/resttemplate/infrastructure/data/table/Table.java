package com.trick.resttemplate.infrastructure.data.table;

import android.net.Uri;

public enum  Table {
	
	//@formatter:off
	Items(ItemTable.class);
	//@formatter:on
	
	
	BaseTable mTableInstance;
	
	Table(Class<? extends BaseTable> tableClass)
	{
		try
		{
			mTableInstance = tableClass.newInstance();
		} catch (InstantiationException e)
		{
			e.printStackTrace();
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public int GetTableMatch()
	{
		return ordinal() * 2;
	}

	public int GetRowMatch()
	{
		return GetTableMatch() + 1;
	}

	public BaseTable getTable()
	{
		return mTableInstance;
	}

	public String getTableName()
	{
		return mTableInstance.getTableName();
	}

	public Uri getContentUri()
	{
		return mTableInstance.getContentUri();
	}

	public static Table getTableFromUriMatch(int match)
	{
		for (Table table : Table.values())
			if (table.GetTableMatch() == match || table.GetRowMatch() == match)
				return table;
		return null;
	}
	

}
