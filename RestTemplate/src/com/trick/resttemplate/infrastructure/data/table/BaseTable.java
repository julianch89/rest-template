package com.trick.resttemplate.infrastructure.data.table;

import com.trick.resttemplate.infrastructure.data.DataProvider;

import android.net.Uri;

public abstract class BaseTable
{
	public abstract String getTableName();

	protected abstract void aditionalColumnsStatement(StringBuilder creationStatement);

	public Uri getContentUri()
	{
		return Uri.parse("content://" + DataProvider.AUTHORITY + "/" + getTableName());
	}
	
	public abstract String getId();
	
	public String createSQL()
	{
		StringBuilder sqlBuilder = new StringBuilder();

		sqlBuilder.append("CREATE TABLE " + getTableName() + " (");
		sqlBuilder.append(ResourceTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, ");
		sqlBuilder.append(ResourceTable._STATUS + " TEXT, ");
		sqlBuilder.append(ResourceTable._RESULT + " INTEGER, ");
		aditionalColumnsStatement(sqlBuilder);

		sqlBuilder.append(");");

		return sqlBuilder.toString();
	}

}
