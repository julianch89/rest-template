package com.trick.resttemplate.infrastructure.data;

import java.util.List;

import com.trick.resttemplate.domain.Item;
import com.trick.resttemplate.infrastructure.data.table.ItemTable;
import com.trick.resttemplate.infrastructure.data.table.Table;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

public class ItemsDbHelper {
	
	
	
	public static void saveItem(Context context , Item item)
	{
		
		ContentValues cv =  new ContentValues();
		cv.put("_STATUS", "");
		context.getContentResolver().update(Uri.withAppendedPath(Table.Items.getContentUri(), item.getId()), cv, null, null);
	}
	
	public static void saveItems(Context context, List<Item> items)
	{
	
		context.getContentResolver().delete(Table.Items.getContentUri(), null, null);
		int i=0;
		ContentValues[] values = new ContentValues[items.size()];
		for(Item item : items)
		{
			ContentValues cv =  new ContentValues();
			cv.put(ItemTable.Column.ITEM_ID, item.getId());
			cv.put(ItemTable.Column.NAME, item.getName());
			values[i++] = cv;
			
		}
		context.getContentResolver().bulkInsert(Table.Items.getContentUri(), values);
		
	}

}
