package com.trick.resttemplate.infrastructure.processor;

import java.util.List;

import android.content.Context;

import com.trick.resttemplate.domain.Item;
import com.trick.resttemplate.infrastructure.data.ItemsDbHelper;
import com.trick.resttemplate.infrastructure.rest.request.base.RequestType;
import com.trick.resttemplate.infrastructure.rest.response.GetItemsResponseContent;
import com.trick.resttemplate.infrastructure.rest.response.IResponseContent;
import com.trick.resttemplate.infrastructure.rest.response.PostItemContent;

public class ItemProcessor implements IBaseProcessor {

	@Override
	public void handleResponse(Context context, RequestType requestId, IResponseContent responseContent)
	{
		
		switch (requestId) {
		case GetItems:
			List<Item> items = ((GetItemsResponseContent)responseContent).getItems();
			ItemsDbHelper.saveItems(context, items);
			break;
		case PostItem:
			Item item = ((PostItemContent)responseContent).getItem();
			ItemsDbHelper.saveItem(context, item);
		default:
			break;
		}
		
		
	}

}
