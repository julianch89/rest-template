package com.trick.resttemplate.infrastructure.processor;

import com.trick.resttemplate.infrastructure.rest.request.base.RequestType;
import com.trick.resttemplate.infrastructure.rest.response.IResponseContent;

import android.content.Context;


public interface IBaseProcessor
{
	public void handleResponse(Context context, RequestType requestId, IResponseContent responseContent);
}
