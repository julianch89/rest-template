package com.trick.resttemplate.infrastructure.processor;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.trick.resttemplate.RestTemplateApp;
import com.trick.resttemplate.domain.Item;
import com.trick.resttemplate.infrastructure.ReflectionHelper;
import com.trick.resttemplate.infrastructure.rest.RestMethodResult;
import com.trick.resttemplate.infrastructure.rest.VolleyErrorHelper;
import com.trick.resttemplate.infrastructure.rest.request.base.BaseJsonRequest;
import com.trick.resttemplate.infrastructure.rest.request.base.RequestProvider;
import com.trick.resttemplate.infrastructure.rest.request.base.RequestType;
import com.trick.resttemplate.infrastructure.rest.response.BaseResponse;
import com.trick.resttemplate.infrastructure.rest.response.ResponseMessage;
import com.trick.resttemplate.infrastructure.rest.service.RequestIntentService;

public class BaseProcessor
{
	protected static final String TAG = BaseProcessor.class.getSimpleName();

	public static Map<Type, Class<? extends IBaseProcessor>> processorsMapping = new HashMap<Type, Class<? extends IBaseProcessor>>();
	static
	{
		processorsMapping.put(Item.class, ItemProcessor.class);
		
	};

	protected Context mContext;

	public BaseProcessor(Context context)
	{
		mContext = context;
	}

	public void queueRequest(RequestType requestId, Bundle parameters, final ProcessorCallback callback)
	{
		BaseJsonRequest req = RequestProvider.createRequest(requestId, parameters,
				getResponseListener(requestId, callback), getErrorListener(callback));
		setDbStatus(req, parameters);
		RestTemplateApp.getInstance().addToRequestQueue(req);
	}

	private Response.ErrorListener getErrorListener(final ProcessorCallback callback)
	{
		return new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(VolleyError error)
			{
				if (error.networkResponse == null)
				{
					Log.e(TAG, "network response is null. " + error.getMessage());
					callback.send(new RestMethodResult(RequestIntentService.REQUEST_FAIL, ResponseMessage.unknownErrorResponse()));
				} else
					callback.send(new RestMethodResult(RequestIntentService.REQUEST_FAIL, VolleyErrorHelper
							.getMessage(error, mContext)));
			}
		};
	}

	private Response.Listener<JSONObject> getResponseListener(final RequestType requestId,
			final ProcessorCallback callback)
	{
		return new Response.Listener<JSONObject>()
		{
			@Override
			public void onResponse(JSONObject response)
			{
				if (requestId.getResponseClass() != null)
				{
					BaseResponse<?> resourceResponse = new Gson().fromJson(response.toString(),
							requestId.getResponseClass());
					IBaseProcessor processor = ReflectionHelper.createInstance(processorsMapping.get(resourceResponse
							.getResponse().getResourceType()));
					processor.handleResponse(mContext, requestId, resourceResponse.getResponse());
				}
			
				callback.send(new RestMethodResult(RequestIntentService.REQUEST_SUCCESS, null));
			}
		};
	}
	
	private void setDbStatus(BaseJsonRequest req, Bundle parameters)
	{
		ContentValues  cv;
		String id;
		switch (req.getMethod()) {
		case Method.POST:
			
			cv = new ContentValues();
			for(String key : parameters.keySet())
			{
				cv.put(key, parameters.getString(key));
			}
			cv.put("_STATUS", "STATE_POSTING");
			mContext.getContentResolver().insert(req.getUri(), cv);
			break;
		case Method.PUT:
			cv = new ContentValues();
			for(String key : parameters.keySet())
			{
				cv.put(key, parameters.getString(key));
			}
			cv.put("_STATUS", "STATE_UPDATING");
			id = String.valueOf(cv.get(req.getTable().getId()));
			
			mContext.getContentResolver().update(Uri.withAppendedPath(req.getUri(), id), cv, null, null);
			break;
		case Method.DELETE:
			cv = new ContentValues();
			for(String key : parameters.keySet())
			{
				cv.put(key, parameters.getString(key));
			}
			cv.put("_STATUS", "STATE_DELETING");
			id = String.valueOf(cv.get(req.getTable().getId()));
			
			mContext.getContentResolver().update(Uri.withAppendedPath(req.getUri(), id), cv, null, null);
			break;
		default:
			break;
		}
	}
}
