package com.trick.resttemplate.infrastructure.processor;

import com.trick.resttemplate.infrastructure.rest.RestMethodResult;


public interface ProcessorCallback
{
	void send(RestMethodResult result);
}
