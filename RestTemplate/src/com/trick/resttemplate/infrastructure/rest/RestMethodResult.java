package com.trick.resttemplate.infrastructure.rest;

import com.trick.resttemplate.infrastructure.rest.response.ResponseMessage;


public class RestMethodResult
{
	private int  mCode;
	private ResponseMessage mResponseMessage;

	public RestMethodResult(int code, ResponseMessage statusMsg)
	{
		super();
		mCode = code;
		mResponseMessage = statusMsg;
		
	}
	
	public int getCode()
	{
		return mCode;
	}

	public ResponseMessage getResponseMessage()
	{
		return mResponseMessage;
	}


}
