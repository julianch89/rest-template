package com.trick.resttemplate.infrastructure.rest.service;



import com.trick.resttemplate.infrastructure.processor.BaseProcessor;
import com.trick.resttemplate.infrastructure.processor.ProcessorCallback;
import com.trick.resttemplate.infrastructure.rest.RestMethodResult;
import com.trick.resttemplate.infrastructure.rest.request.base.RequestType;
import com.trick.resttemplate.infrastructure.rest.response.ResponseMessage;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

public class RequestIntentService extends IntentService{
	
	public static final String PACKAGE_NAME = "com.trick.resttempalte";
	
	/** Extra: Unique request type identifier, e.g. Login, SignUp, etc... */
	public static final String API_REQUEST_TYPE_ID = PACKAGE_NAME + "API_REQUEST_TYPE";

	/** Extra: Callback for request result (ResultReceiver) */
	public static final String SERVICE_CALLBACK =  PACKAGE_NAME + "SERVICE_CALLBACK";

	/** Extra: Intent that triggered the request */
	public static final String ORIGINAL_INTENT =  PACKAGE_NAME + "ORIGINAL_INTENT";

	/** Extra: Error Message */
	public static final String EXTRA_ERROR_MESSAGE =  PACKAGE_NAME + "EXTRA_ERROR_MESSAGE";

	/** Extra: Parameters bundle */
	public static final String EXTRA_PARAMETERS_BUNDLE =  PACKAGE_NAME + "EXTRA_PARAMETERS_BUNDLE";

	public static final int REQUEST_FAIL = 0;
	public static final int REQUEST_SUCCESS = 1;
	public static final int REQUEST_TIME_OUT = 2;
	
	/** Callback to send request result */
	private ResultReceiver mCallback;

	/** Intent that triggered the request */
	private Intent mOriginalRequestIntent;
	
	private BaseProcessor mBaseProcessor;
	
	public RequestIntentService()
	{
		super("RequestIntentService");
		mBaseProcessor = new BaseProcessor(RequestIntentService.this);
		
	}

	@Override
	protected void onHandleIntent(Intent requestIntent)
	{
		mOriginalRequestIntent = requestIntent;
		mCallback = requestIntent.getParcelableExtra(RequestIntentService.SERVICE_CALLBACK);

		int method = requestIntent.getIntExtra(RequestIntentService.API_REQUEST_TYPE_ID, -1);
		Bundle parameters = requestIntent.getBundleExtra(RequestIntentService.EXTRA_PARAMETERS_BUNDLE);
		mBaseProcessor.queueRequest(RequestType.values()[method], parameters, makeProcessorCallback());
	}
	
	/*** Notify the caller via callback, using provided ResultReceiver */
	private ProcessorCallback makeProcessorCallback()
	{
		ProcessorCallback callback = new ProcessorCallback()
		{
			@Override
			public void send(RestMethodResult result)
			{
				if (mCallback != null)
					mCallback.send(result.getCode(), getOriginalIntentBundle(result.getResponseMessage()));
			}
		};
		return callback;
	}

	/** Returns a bundle with the intent who originally started the request */
	protected Bundle getOriginalIntentBundle(ResponseMessage responseMessage)
	{
		Bundle originalRequest = new Bundle();
		originalRequest.putParcelable(ORIGINAL_INTENT, mOriginalRequestIntent);
		originalRequest.putParcelable(EXTRA_ERROR_MESSAGE, responseMessage);
		return originalRequest;
	}

}
