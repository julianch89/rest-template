package com.trick.resttemplate.infrastructure.rest.response;

import java.lang.reflect.Type;

import com.trick.resttemplate.domain.Item;



public class PostItemContent extends Item implements IResponseContent {

	
	public Item getItem()
	{
		return this;
	}
	
	@Override
	public Type getResourceType()
	{
		return Item.class;
	}

}
