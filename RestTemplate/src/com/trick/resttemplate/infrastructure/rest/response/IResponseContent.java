package com.trick.resttemplate.infrastructure.rest.response;

import java.lang.reflect.Type;

public interface IResponseContent
{
	public Type getResourceType();
}
