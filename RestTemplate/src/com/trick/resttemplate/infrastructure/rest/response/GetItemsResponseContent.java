package com.trick.resttemplate.infrastructure.rest.response;

import java.lang.reflect.Type;
import java.util.ArrayList;


import java.util.List;

import com.trick.resttemplate.domain.Item;

public class GetItemsResponseContent extends ArrayList<Item> implements IResponseContent
{
	


	public List<Item> getItems()
	{
		return this;
	}
	
	@Override
	public Type getResourceType()
	{
		return Item.class;
		
	}

}
