package com.trick.resttemplate.infrastructure.rest.response;

import com.google.gson.annotations.SerializedName;


public class BaseResponse<T extends IResponseContent>
{
	
	@SerializedName("data")
	private T mResponse;

	@SerializedName("success")
	private int mSucces;

	public T getResponse()
	{
		return mResponse;
	}

}
