package com.trick.resttemplate.infrastructure.rest.request.base;



import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;
import com.trick.resttemplate.infrastructure.rest.request.GetItemsRequest;
import com.trick.resttemplate.infrastructure.rest.request.PostItemRequest;
import com.trick.resttemplate.infrastructure.rest.response.BaseResponse;
import com.trick.resttemplate.infrastructure.rest.response.GetItemsResponseContent;
import com.trick.resttemplate.infrastructure.rest.response.PostItemContent;

public enum RequestType {

	//@formatter:off
	
	GetItems(GetItemsRequest.class,new TypeToken<BaseResponse<GetItemsResponseContent>>(){}.getType()),
	PostItem(PostItemRequest.class,new TypeToken<BaseResponse<PostItemContent>>(){}.getType());
	//@formatter:on
	
	
	RequestType(Class<?> requestClass, Type responseClass)
	{
		mRequestClass = requestClass;
		mResponseClass = responseClass;
	}

	/** Class that handle the request */
	public Class<?> getRequestClass()
	{
		return mRequestClass;
	}

	/** Class that handle the request's response */
	public Type getResponseClass()
	{
		return mResponseClass;
	}

	private Class<?> mRequestClass;
	private Type mResponseClass;
	
}
