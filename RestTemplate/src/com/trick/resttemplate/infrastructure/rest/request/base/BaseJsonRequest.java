package com.trick.resttemplate.infrastructure.rest.request.base;


import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.trick.resttemplate.LogHelper;
import com.trick.resttemplate.infrastructure.data.table.BaseTable;
import com.trick.resttemplate.infrastructure.rest.ParametersBundle;



/**
 * Extends JsonObjectRequest to simplify JSON Requests handling.
 * 
 * <p>
 * Adds extra logging and simplifies parameter handling.
 * 
 * */
public abstract class BaseJsonRequest extends JsonObjectRequest
{
	protected static final String TAG = BaseJsonRequest.class.getSimpleName();

	public static class Parameters
	{
		// Global parameters
		public static final String PAGE_NUMBER = "page";
		public static final String PAGE_SIZE = "itemsPerPage";
	}

	private boolean mRequiresSessionToken;
	
	public abstract int getMethod();
	
	public abstract Uri getUri();
	
	public abstract BaseTable  getTable();

	/**
	 * @param method
	 *            HTTP method type (POST, PUT, DELETE or GET)
	 * 
	 * @param url
	 *            Full URL for the request
	 * 
	 * @param parameters
	 *            Bundle with required and optional parameters
	 * 
	 * @param listener
	 *            Listener to handle request successful response
	 * 
	 * @param errorListener
	 *            Listener to handle request error response
	 * 
	 * @param parametersClass
	 *            Static class describing request parameters
	 * */
	protected BaseJsonRequest(int method, String url, Bundle parameters, Listener<JSONObject> listener,
			ErrorListener errorListener)
	{
		super(method, url, new JSONObject(ParametersBundle.toMap(parameters)), listener, errorListener);
		if (LogHelper.LOG_DEBUG_INFO)
			Log.i(TAG, RequestLoggingHelper.getRequestText(this));
	}

	protected BaseJsonRequest(int method, String url, Bundle parameters, Listener<JSONObject> listener,
			ErrorListener errorListener, boolean requiresSessionToken)
	{
		this(method, url, parameters, listener, errorListener);
		mRequiresSessionToken = requiresSessionToken;
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError
	{
		Map<String, String> headers = new HashMap<String, String>();
		//TODO set apiKEy
		//headers.put("apikey", "Wq8b5ANU78TROK7hRqxOYrq0B0iRRY_android");
		headers.put("Content-Type", "application/json");

		if (mRequiresSessionToken)
			headers.put("token", getSessionToken());

		return headers;
	}

	@Override
	public String getBodyContentType()
	{
		return "application/json";
	}

	@Override
	public void deliverError(VolleyError error)
	{
		if (LogHelper.LOG_DEBUG_INFO)
			Log.i(TAG, RequestLoggingHelper.getRequestErrorText(this, error));
		super.deliverError(error);
	}

	@Override
	protected void deliverResponse(JSONObject response)
	{
		if (LogHelper.LOG_DEBUG_INFO)
			Log.i(TAG, RequestLoggingHelper.getRequestResponseText(this, response));
		super.deliverResponse(response);
	}

	private String getSessionToken()
	{
		//TODO return session token 
		return "sessionTokenExample";
		
	}

	/** Used to add parameters on URL */
	protected static String getUrlWithParameters(Bundle parameters)
	{
		StringBuilder urlParameters = new StringBuilder();
		Map<String, String> parametersMap = ParametersBundle.toMap(parameters);

		if (parametersMap.size() > 0)
			urlParameters.append("?");

		for (String parameterName : parametersMap.keySet())
			urlParameters.append(parameterName).append("=").append(parametersMap.get(parameterName)).append("&");

		urlParameters.deleteCharAt(urlParameters.length() - 1);

		return urlParameters.toString();
	}
}