package com.trick.resttemplate.infrastructure.rest.request;

import org.json.JSONObject;

import android.net.Uri;
import android.os.Bundle;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.trick.resttemplate.infrastructure.data.table.BaseTable;
import com.trick.resttemplate.infrastructure.data.table.Table;
import com.trick.resttemplate.infrastructure.rest.RestConstants;
import com.trick.resttemplate.infrastructure.rest.request.base.BaseJsonRequest;

public class GetItemsRequest extends BaseJsonRequest {
	
	private static final String relativeUrl = "/items.php";
	
	
	public GetItemsRequest(Bundle parameters, Listener<JSONObject> listener, ErrorListener errorListener)
	{
		super(Method.GET,RestConstants.API_URL+relativeUrl,null,listener,errorListener);
	}
	

	
	@Override
	public int getMethod()
	{
		return Method.GET;
	}
	
	@Override
	public Uri getUri()
	{
		return Table.Items.getContentUri();
	}
	
	@Override
	public BaseTable getTable()
	{
		return Table.Items.getTable();
	}
	
	

}
