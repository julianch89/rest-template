package com.trick.resttemplate.infrastructure.rest.request;

import org.json.JSONObject;

import android.net.Uri;
import android.os.Bundle;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.trick.resttemplate.infrastructure.data.table.BaseTable;
import com.trick.resttemplate.infrastructure.data.table.Table;
import com.trick.resttemplate.infrastructure.rest.RestConstants;
import com.trick.resttemplate.infrastructure.rest.request.base.BaseJsonRequest;

public class PostItemRequest extends BaseJsonRequest{
	
	
	private static final String relativeUrl = "/postItem.php";
	
	public PostItemRequest (Bundle parameters, Listener<JSONObject> listener, ErrorListener errorListener)
	{
		super(Method.POST,RestConstants.API_URL+relativeUrl,parameters,listener,errorListener);
	}
	
	

	public static class Parameters
	{
		// Required parameters
		public static final String ID = "item_id";
		public static final String NAME = "name";

	
	}

	@Override
	public int getMethod()
	{
		return Method.POST;
	}

	@Override
	public Uri getUri()
	{
		return Table.Items.getContentUri();
	}

	@Override
	public BaseTable getTable()
	{
		return Table.Items.getTable();
	}

}
