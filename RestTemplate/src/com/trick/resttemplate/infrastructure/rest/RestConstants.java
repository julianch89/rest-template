package com.trick.resttemplate.infrastructure.rest;

public class RestConstants
{
	public static final String API_URL = "http://www.trickgs.com/golf";

	public enum HttpStatus
	{
		//@formatter:off
		OK_200(200), 
		CREATED_201(201), 
		ACCEPTED_202(202), 
		BAD_REQUEST_400(400), 
		UNAUTHORIZED_401(401),
		FORBIDDEN_403(403),
		NOT_FOUND_404(404),
		REQUEST_TIMEOUT_408(408),
		LENGTH_REQUIRED_411(411),
		PRECONDITION_FAILED_412(412);
		//@formatter:on

		private int mCode;

		HttpStatus(int code)
		{
			mCode = code;
		}

		public int getCode()
		{
			return mCode;
		}

		public static HttpStatus from(int code)
		{
			for (HttpStatus status : HttpStatus.values())
			{
				if (status.getCode() == code)
					return status;
			}
			return null;
		}
	}
}
