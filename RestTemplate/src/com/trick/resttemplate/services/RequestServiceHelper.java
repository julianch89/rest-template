package com.trick.resttemplate.services;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.trick.resttemplate.RestTemplateApp;
import com.trick.resttemplate.infrastructure.rest.ParametersBundle;
import com.trick.resttemplate.infrastructure.rest.request.PostItemRequest;
import com.trick.resttemplate.infrastructure.rest.request.base.RequestType;
import com.trick.resttemplate.infrastructure.rest.response.ResponseMessage;
import com.trick.resttemplate.infrastructure.rest.service.RequestIntentService;

public class RequestServiceHelper {

	/** Id used to broadcast request result */
	public static final String ACTION_REQUEST_RESULT = "ACTION_REQUEST_RESULT";

	/** Extra: Auto-generated unique request identifier */
	public static final String EXTRA_REQUEST_TRANSACTION_ID = "EXTRA_REQUEST_TRANSACTION_ID";

	/** Extra: Request result code, e.g. 200 (Ok), 400 (Bad Request) */
	public static final String EXTRA_RESULT_CODE = "EXTRA_RESULT_CODE";

	/** Extra: Request result message */
	public static final String EXTRA_RESULT_MSG = "EXTRA_RESULT_MSG";

	/** Auto-generated unique request identifier */
	private static long generateRequestID()
	{
		return UUID.randomUUID().getLeastSignificantBits();
	}

	/** Context used for Broadcast and Services */
	private static Context getContext()
	{
		return RestTemplateApp.getInstance().getApplicationContext();
	}

	/** Callback to handle Request result */
	private static ResultReceiver mServiceCallback = new ResultReceiver(null) {
		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData)
		{
			handleRequestResponse(resultCode, resultData);
		}
	};

	/**
	 * Receives request result and Broadcast result information
	 * 
	 * @param resultCode
	 *            Request result (HTTP status code)
	 * 
	 * @param resultData
	 *            Bundle with extra information about request result
	 * */
	private static void handleRequestResponse(int resultCode, Bundle resultData)
	{
		Intent originalIntent = (Intent) resultData.getParcelable(RequestIntentService.ORIGINAL_INTENT);
		ResponseMessage msg = resultData.getParcelable(RequestIntentService.EXTRA_ERROR_MESSAGE);

		if (originalIntent != null)
		{
			long requestId = originalIntent.getLongExtra(EXTRA_REQUEST_TRANSACTION_ID, 0);

			Intent resultBroadcast = new Intent(ACTION_REQUEST_RESULT);
			resultBroadcast.putExtra(EXTRA_REQUEST_TRANSACTION_ID, requestId);
			resultBroadcast.putExtra(EXTRA_RESULT_CODE, resultCode);
			resultBroadcast.putExtra(EXTRA_RESULT_MSG, msg);

			getContext().sendBroadcast(resultBroadcast);
		}
	}

	/**
	 * Creates an Intent with request type and parameters and send it to
	 * RequestIntentService.
	 * 
	 * @param requestType
	 *            Unique identifier for API request
	 * 
	 * @param parameters
	 *            Parameters needed to make API request
	 * 
	 * @return Unique ID to identify the transaction
	 * */
	private static long requestToService(RequestType requestType, Map<String, String> parameters)
	{
		long requestId = generateRequestID();

		Intent intent = new Intent(getContext(), RequestIntentService.class);
		intent.putExtra(RequestIntentService.API_REQUEST_TYPE_ID, requestType.ordinal());
		intent.putExtra(RequestIntentService.SERVICE_CALLBACK, mServiceCallback);
		intent.putExtra(RequestIntentService.EXTRA_PARAMETERS_BUNDLE, ParametersBundle.fromMap(parameters));
		intent.putExtra(EXTRA_REQUEST_TRANSACTION_ID, requestId);

		getContext().startService(intent);
		return requestId;
	}

	public static long getItems()
	{
		HashMap<String, String> parameters = new HashMap<String, String>();
		return requestToService(RequestType.GetItems, parameters);

	}
	
	public static long postItem()
	{
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(PostItemRequest.Parameters.ID, "50");
		parameters.put(PostItemRequest.Parameters.NAME, "Test Item");
		return requestToService(RequestType.PostItem, parameters);
	}
	
}
