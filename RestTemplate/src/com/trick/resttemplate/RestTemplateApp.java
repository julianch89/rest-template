package com.trick.resttemplate;



import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import android.app.Application;
import android.text.TextUtils;

public class RestTemplateApp extends Application{
	
	
	public static final String TAG = RestTemplateApp.class.getSimpleName();
	
	private static RestTemplateApp mInstance;
	
	private RequestQueue mRequestQueue;
	
	public static RestTemplateApp getInstance()
	{
		return mInstance;
	}

	
	@Override
	public void onCreate()
	{
		
		super.onCreate();
		mInstance = this;
		
	}
	
	public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
 
        return mRequestQueue;
    }
	
	public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }
 
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
 
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
