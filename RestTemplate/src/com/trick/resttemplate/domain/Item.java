package com.trick.resttemplate.domain;

import com.google.gson.annotations.SerializedName;
import com.trick.resttemplate.infrastructure.rest.Resource;

public class Item implements Resource {
	
	@SerializedName("id")
	public String mId;
	@SerializedName("name")
	public String mName;
	
	public String getId()
	{
		return mId;
	}
	public String getName()
	{
		return mName;
	}

}
